import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/login/login.service';
import * as firebase from "firebase";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';

@Component({
  selector: 'app-repost',
  templateUrl: './repost.component.html',
  styleUrls: ['./repost.component.css'],
  providers:[LoginService]
})
export class RepostComponent implements OnInit {
    public lookForGroup: FirebaseListObservable<any>;
    public isLoggedIn: boolean;
    public isVerified: boolean;
    public key;

  constructor(public af: AngularFire,public loginService: LoginService) {
    this.lookForGroup = this.af.database.list('LFG');
    this.loginService.af.auth.subscribe(
      (auth) => {
        if(auth == null) {
            this.isLoggedIn = false;
        }
        else {
        this.isLoggedIn = true;
          if(!firebase.auth().currentUser.emailVerified){
                this.isVerified = false;
              }
          else{
              this.isVerified = true;
            }
          if(auth.google) {
            this.loginService.displayName = auth.google.displayName;
            this.loginService.email = auth.google.email;
            console.log(this.loginService.email);
          }
          else {
            this.loginService.displayName = auth.auth.email;
            this.loginService.email = auth.auth.email;
            console.log(this.loginService.email);
          }
        }
      }
    );
    
  }

  ngOnInit() {
  }
  update(key,gameStus,PlatformStus,type, match, starttime, min, newmessage, email){
    
    var message = {
      gamStus: gameStus,
      PlatformStus: PlatformStus,
      type: type,
      match: match,
      starttime: starttime,
      min: min,
      description: newmessage,
      email: email,
      timestamp: Date.now(),
    };
    console.log(message);
    this.lookForGroup.update(key,message);
  }
    delete(key){
    this.lookForGroup.remove(key);
  }

}
