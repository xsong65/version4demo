import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookForGroupComponent } from './look-for-group.component';

describe('LookForGroupComponent', () => {
  let component: LookForGroupComponent;
  let fixture: ComponentFixture<LookForGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookForGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookForGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
