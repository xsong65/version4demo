import { Component, OnInit } from '@angular/core';
import * as firebase from "firebase";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';
import { ProfileService } from 'app/profile/profile.service';
import { LoginService } from 'app/login/login.service';

@Component({
  selector: 'app-look-for-group',
  templateUrl: './look-for-group.component.html',
  styleUrls: ['./look-for-group.component.css'],
  providers:[ProfileService,LoginService]
})
export class LookForGroupComponent implements OnInit {
  
  public lookForGroup: FirebaseListObservable<any>;
  public isLoggedIn: boolean;
  public isVerified: boolean;
  public time = Date.now();
  public profiles: FirebaseListObservable<any>;
  public idpc;
  public idps3;
  public idps4;
  public idxbox;
  public idxb1;
  public filtered:boolean=false;
  public filteredResult;
  public result=[];
  public fcomb: boolean=false;


 
  constructor(public af: AngularFire,private profileService: ProfileService,public loginService: LoginService) {
    this.lookForGroup = this.af.database.list('LFG');
    this.profiles = this.af.database.list('profile');

    //console.log(this.lookForGroup);
    this.loginService.af.auth.subscribe(
      (auth) => {
        if(auth == null) {
            this.isLoggedIn = false;
        }
        else {
        this.isLoggedIn = true;
          if(!firebase.auth().currentUser.emailVerified){
                this.isVerified = false;
              }
          else{
              this.isVerified = true;
            }
          if(auth.google) {
            this.loginService.displayName = auth.google.displayName;
            this.loginService.email = auth.google.email;
            console.log(this.loginService.email);
          }
          else {
            this.loginService.displayName = auth.auth.email;
            this.loginService.email = auth.auth.email;
            console.log(this.loginService.email);
          }
        }
      }
    );
    this.profiles.forEach(element => {
      //console.log('1111');
       element.forEach(item=>{
         //console.log(item);
         //console.log(this.loginService.email);
         if (item.userEmail==this.loginService.email){
           console.log('3333');
           //console.log(item.useridPC)
           this.idpc=item.useridPC;
           this.idps3=item.useridPS3;
           this.idps4=item.useridPS4;
           this.idxbox=item.useridXBox;
           this.idxb1=item.useridXB1;

         }
       }
         );
        });
    
  }
  


  ngOnInit() {
  }
  
  post(gameStus,PlatformStus,type, match, starttime, min, newmessage, email){
    
    var message = {
      gamStus: gameStus,
      PlatformStus: PlatformStus,
      type: type,
      match: match,
      starttime: starttime,
      min: min,
      description: newmessage,
      email: email,
      timestamp: Date.now(),
      useridPC: this.idpc,
      useridPS3: this.idps3,
      useridPS4: this.idps4,
      useridXBox: this.idxbox,
      useridXB1: this.idxb1
    };
    console.log(message);
    this.lookForGroup.push(message);
  }
  
  filtergame(game){

    this.filtered=true;
    this.fcomb=false;
    this.filteredResult = this.af.database.list('LFG',{
      query:{
        orderByChild: 'gamStus',

        equalTo: game
      }
    });
  }
  filterplatform(platform){
    this.filtered=true;
    this.fcomb=false;
    this.filteredResult = this.af.database.list('LFG',{
      query:{
        orderByChild: 'PlatformStus',

        equalTo: platform
      }
    });
    
  }
  filtertime(time){
    console.log(time);
    this.filtered=true;
    this.fcomb=false;
    this.filteredResult = this.af.database.list('LFG',{
      query:{
        orderByChild: 'starttime',

        equalTo: time
      }
    });
  }
  
  combination(game,platform,time){
    this.result=[];
    //console.log(game);
    //console.log(platform);
    //console.log(time);
    this.filtered=true;
    this.fcomb=true;
    this.lookForGroup.forEach(element => {
       element.forEach(item=>{
         if(game=='select'){
            if (item.PlatformStus==platform&&item.starttime==time){
                this.result.push(item);
            }
         }else if(platform=='select'){
            if(item.gamStus==game&&item.starttime==time){
              this.result.push(item);
            }
         }else if(time==''){
           if(item.gamStus==game&&item.PlatformStus==platform){
              this.result.push(item);
            }
         }
         else{
           if(item.gamStus==game&&item.PlatformStus==platform&&item.starttime==time){
              this.result.push(item);
            }
         }
       }
         );
        });
    
  }

}
