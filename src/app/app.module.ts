import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import {RouterModule, Routes} from "@angular/router";

import { AppComponent } from './app.component';
import { FrontpageComponent } from './frontpage/frontpage.component';
import { LoginComponent } from './login/login.component';
import { LookForGroupComponent } from './look-for-group/look-for-group.component';
import { CommercialComponent } from './commercial/commercial.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './login/register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthService } from './login/auth.service';
import { GamesComponent } from './games/games.component';
import { DestinyDetailComponent } from './games/destiny-detail/destiny-detail.component';
import { Diablo3DetailComponent } from './games/diablo3-detail/diablo3-detail.component';
import { Left4dead2DetailComponent } from './games/left4dead2-detail/left4dead2-detail.component';
import { WorldofwarcraftDetailComponent } from './games/worldofwarcraft-detail/worldofwarcraft-detail.component';
import { RepostComponent } from './look-for-group/repost/repost.component';

export const firebaseConfig = {
  apiKey: 'AIzaSyDZ0lg0uW3NaHirrIgzPVF0u4DyxvuuR5U',
  authDomain: 'proj-ed9c1.firebaseapp.com',
  databaseURL: 'https://proj-ed9c1.firebaseio.com',
  storageBucket: 'proj-ed9c1.appspot.com'
};

const routes: Routes = [
  { path: '', component: FrontpageComponent },
  { path: 'login', component: LoginComponent},
  { path: 'lookforgroup', component: LookForGroupComponent},
  { path: 'register', component:RegisterComponent},
  { path: 'profile', component: ProfileComponent,canActivate: [AuthService]},
  { path: 'games', component: GamesComponent},
  { path: 'destiny', component: DestinyDetailComponent },
  { path: 'diablo3', component: Diablo3DetailComponent },
  { path: 'left4dead2',component: Left4dead2DetailComponent },
  { path: 'worldofwarcraft', component: WorldofwarcraftDetailComponent },
  { path: 'repost', component:RepostComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    FrontpageComponent,
    LoginComponent,
    LookForGroupComponent,
    CommercialComponent,
    HeaderComponent,
    RegisterComponent,
    ProfileComponent,
    GamesComponent,
    DestinyDetailComponent,
    Diablo3DetailComponent,
    Left4dead2DetailComponent,
    WorldofwarcraftDetailComponent,
    RepostComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(routes)
  ],
  
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
