import { Component } from '@angular/core';
import { LoginService } from 'app/login/login.service';
import { Router } from "@angular/router";
import * as firebase from "firebase";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [LoginService]
})
export class AppComponent {
    public error: any;
    public userName;
    public isLoggedIn: boolean;
    public isVerified: boolean;
    constructor(public loginService: LoginService,private router: Router) {
     this.loginService.af.auth.subscribe(
      (auth) => {
        if(auth == null) {
          console.log("Not Logged in.");
          //this.router.navigate(['login']);
          this.isLoggedIn = false;
        }
        else {
          //console.log(this.loginService.isLoggedIn);
          console.log("Successfully Logged in.");
          this.isLoggedIn = true;
          
          if(!firebase.auth().currentUser.emailVerified)
              {
                //console.log("not verified");
                  this.isVerified = false;
              }
            else{
              //console.log("verified"); 
              this.isVerified = true;
            }
          // Set the Display Name and Email so we can attribute messages to them
          if(auth.google) {
            this.loginService.displayName = auth.google.displayName;
            this.loginService.email = auth.google.email;
          }
          else {
            this.loginService.displayName = auth.auth.email;
            this.loginService.email = auth.auth.email;
          }

          // UPDATE: I forgot this at first. Without it when a user is logged in and goes directly to /login
          // the user did not get redirected to the home page.
          this.router.navigate(['']);
        }
      }
    );
    //if (this.isLoggedIn==this.loginService.isLoggedIn){
      //console.log('AAAAAAa');
    //}
    
    
  }
  
   logout() {
    this.loginService.logout();
    this.isVerified = false;
    this.isLoggedIn = false;
  }
}
