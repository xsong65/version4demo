import { Injectable } from '@angular/core';
import {AngularFire, AuthProviders, AuthMethods} from 'angularfire2';

@Injectable()
export class LoginService {
  //public isLoggedIn: boolean;
  //public users: FirebaseListObservable<any>;
  public displayName: string;
  public email: string;
  //public user: FirebaseObjectObservable<any>;

  constructor(public af: AngularFire) {}
  /**
   * Logs in the user
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  loginWithGoogle() {
    //this.isLoggedIn = true;
    //console.log(this.isLoggedIn);
    return this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup,
    });
  }
  /**
   * Calls the AngularFire2 service to register a new user
   * @param model
   * @returns {firebase.Promise<void>}
   */
  registerUser(email, password) {
    console.log(email);
    //this.isLoggedIn = true;
    //console.log(this.isLoggedIn);
    return this.af.auth.createUser({
      email: email,
      password: password
    });
  }
  /**
   * Saves information to display to screen when user is logged in
   * @param uid
   * @param model
   * @returns {firebase.Promise<void>}
   */
  saveUserInfoFromForm(uid, name, email) {
    return this.af.database.object('registeredUsers/' + uid).set({
      name: name,
      email: email,
    });
  }
   /**
   * Logs the user in using their Email/Password combo
   * @param email
   * @param password
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  loginWithEmail(email, password) {
    //this.isLoggedIn = true;
    //console.log(this.isLoggedIn);
    return this.af.auth.login({
        email: email,
        password: password,
      },
      {
        provider: AuthProviders.Password,
        method: AuthMethods.Password,
      });
  }
  /**
   * Logs out the current user
   */
  logout() {
    return this.af.auth.logout();
  }

}
