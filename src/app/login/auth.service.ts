import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AngularFireAuth } from "angularfire2/angularfire2";
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class AuthService implements CanActivate{

  constructor(private auth: AngularFireAuth, private router: Router,private af: AngularFire) { }
  canActivate(): Observable<boolean> {
  return this.af.auth
    .take(1)
    .map(auth => auth.auth.emailVerified)
    .do(emailVerified => {
      if (!emailVerified) this.router.navigate(['']);
    });
}

}
