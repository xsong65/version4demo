import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from "@angular/router";
import * as firebase from "firebase";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  public error: any;
  public userName;

  constructor(public loginService: LoginService,private router: Router) {  }
    login() {
    this.loginService.loginWithGoogle().then((data) => {
      // Send them to the homepage if they are logged in
        this.router.navigate(['']);
        console.log(data);
        this.userName = data.auth.displayName;
        console.log(this.userName);
        });
      }
      
    loginWithEmail(event, email, password){
    event.preventDefault();
    this.loginService.loginWithEmail(email, password).then(() => {
      this.router.navigate(['']);
    })
      .catch((error: any) => {
        if (error) {
          this.error = error;
          console.log(this.error);
        }
      });
  }

  ngOnInit() {
  }
  
  loginWithGoogle(){
    this.loginService.loginWithGoogle().then((data) =>{
    console.log(data);
    this.userName = data.auth.displayName;
    console.log(this.userName);
    this.router.navigate(['']);
    });
  }
  
  resetPassword(email) {
    console.log("reset button ok");
    firebase.auth().sendPasswordResetEmail(email).then(function() {
      // Email sent.
      console.log("success");
    }, function(error) {
      // An error happened.
      console.log("error" + error);
    });
  }
 

}
