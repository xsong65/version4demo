import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { FormsModule } from '@angular/forms'; 
import { FormControl, FormGroup,Validators,FormBuilder } from '@angular/forms';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

import { LoginService } from 'app/login/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public error: any; 
  state: string = '';


  constructor(private loginService: LoginService, private router: Router,public af: AngularFire) { }
  
  

  ngOnInit() {
  }
  /*
  	//registers the user and logs them in
  register(event, name, email, password) {
    event.preventDefault();
    this.loginService.registerUser(email, password).then((user) => {
      this.loginService.saveUserInfoFromForm(user.uid, name, email).then(() => {
        this.router.navigate(['']);
      })
        .catch((error) => {
          this.error = error;
        });
    })
      .catch((error) => {
        this.error = error;
        console.log(this.error);
      });
  }
  */
  
  onSubmit(formData) {
    if(formData.valid) {
        console.log(formData.value);
        
        
        this.af.auth.createUser({
            email: formData.value.email,
            password: formData.value.password
        }).then(
            authState => {
                authState.auth.sendEmailVerification();
                this.router.navigate([''])
            }).catch(
                (err) => {
                console.log(err);
                this.error = err;
            })
        }
    }

}
