import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Left4dead2DetailComponent } from './left4dead2-detail.component';

describe('Left4dead2DetailComponent', () => {
  let component: Left4dead2DetailComponent;
  let fixture: ComponentFixture<Left4dead2DetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Left4dead2DetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Left4dead2DetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
