import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/login/login.service';
import * as firebase from "firebase";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';


@Component({
  selector: 'app-worldofwarcraft-detail',
  templateUrl: './worldofwarcraft-detail.component.html',
  styleUrls: ['./worldofwarcraft-detail.component.css']
})
export class WorldofwarcraftDetailComponent implements OnInit {

  public isLoggedIn: boolean;
  public isVerified: boolean;
  public profiles: FirebaseListObservable<any>;
  public playList;
  public key;
  public playListDB: FirebaseListObservable<any>;
  public url;
  public stus:boolean;
  public lookForGroup: FirebaseListObservable<any>;
  public pc=[];
  public ps3=[];
  public ps4=[];
  public xbox=[];
  public xb1=[];

  constructor(public loginService: LoginService,public af: AngularFire) { 
    this.profiles = this.af.database.list('profile');
    this.lookForGroup = this.af.database.list('LFG');
    this.loginService.af.auth.subscribe(
      (auth) => {
        if(auth == null) {
          this.isLoggedIn = false;
        }
        else {
          this.isLoggedIn = true;
          
          if(!firebase.auth().currentUser.emailVerified){
                  this.isVerified = false;
              }
          else{
              this.isVerified = true;
            }
          if(auth.google) {
              this.loginService.displayName = auth.google.displayName;
              this.loginService.email = auth.google.email;
          }
          else {
              this.loginService.displayName = auth.auth.email;
              this.loginService.email = auth.auth.email;
          }
        }
      }
    );
    this.profiles.forEach(element => {
       element.forEach(item=>{
         if (item.userEmail==this.loginService.email){
           //console.log(item.userPlaylist);
           this.playList=item.userPlaylist;
           this.key=item.$key;
           console.log(this.key);
         }
       }
         );
        });
      this.url='https://proj-ed9c1.firebaseio.com/profile/';
      this.playListDB=af.database.list(this.url);

this.lookForGroup.forEach(element => {
       element.forEach(item=>{
         if (item.gamStus=="Destiny"){
           if(item.PlatformStus=="PC"){
             this.pc.push(item.useridPC);
           }
           else if(item.PlatformStus=="PS3"){
             this.pc.push(item.useridPS3);
           }
           else if(item.PlatformStus=="PS4"){
             this.pc.push(item.useridPS4);
           }
           else if(item.PlatformStus=="XBOX"){
             this.pc.push(item.useridXBox);
           }
           else if(item.PlatformStus=="XB1"){
             this.pc.push(item.useridXB1);
           }
         }
       }
         );
        });

  }
  addGame(){
    this.playList.forEach(game=>{
      //console.log(game);
      if(game=='World of Warcraft'){
        this.stus=true;
        console.log('already added');
      }
    });
    if(this.stus!=true){
    this.playList.push('World of Warcraft');
      console.log('add success');
    }
    //console.log(this.playList);
    this.playListDB.update(this.key,{userPlaylist:this.playList});
  }

  ngOnInit() {
  }

}
