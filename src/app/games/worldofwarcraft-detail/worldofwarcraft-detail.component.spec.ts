import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldofwarcraftDetailComponent } from './worldofwarcraft-detail.component';

describe('WorldofwarcraftDetailComponent', () => {
  let component: WorldofwarcraftDetailComponent;
  let fixture: ComponentFixture<WorldofwarcraftDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldofwarcraftDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldofwarcraftDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
