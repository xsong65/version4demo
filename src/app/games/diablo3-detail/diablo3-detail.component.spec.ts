import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Diablo3DetailComponent } from './diablo3-detail.component';

describe('Diablo3DetailComponent', () => {
  let component: Diablo3DetailComponent;
  let fixture: ComponentFixture<Diablo3DetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Diablo3DetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Diablo3DetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
