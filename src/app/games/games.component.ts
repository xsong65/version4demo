import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
    public games: FirebaseListObservable<any[]>;
    public destiny: FirebaseListObservable<any>;
    public diablo3: FirebaseListObservable<any>;
    public left4dead2: FirebaseListObservable<any>;
    public worldofwarcraft: FirebaseListObservable<any>;
    
    public destinyCnt;
    public diablo3Cnt;
    public left4dead2Cnt;
    public worldofwarcraftCnt;

  constructor(af: AngularFire) {
    this.games = af.database.list('/games');
    this.destiny = af.database.list('Destiny');
    this.diablo3 = af.database.list('Diablo3');
    this.left4dead2 = af.database.list('Left4Dead2');
    this.worldofwarcraft = af.database.list('WorldofWarcraft');
    
    this.destiny.subscribe((res) => {
    this.destinyCnt = res;
    //console.log(this.destinyCnt.length);
    });
    this.diablo3.subscribe((res) => {
    this.diablo3Cnt = res;
    //console.log(this.diablo3Cnt.length);
    });
    this.left4dead2.subscribe((res) => {
    this.left4dead2Cnt = res;
    //console.log(this.left4dead2Cnt.length);
    });
    this.worldofwarcraft.subscribe((res) => {
    this.worldofwarcraftCnt = res;
    //console.log(this.worldofwarcraftCnt.length);
    })
  }

  ngOnInit() {
  }

}
