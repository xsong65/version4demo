import { Component, OnInit } from '@angular/core';
import { ProfileService } from './profile.service';
import { Observable } from 'rxjs';
import * as firebase from "firebase";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';
import { LoginService } from 'app/login/login.service';


interface Image {
    path: string;
    filename: string;
    downloadURL?: string;
    $key?: string;
}

var imageSize = 10000;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ProfileService]
})
export class ProfileComponent implements OnInit {
  //userEmail:string;
  
  playtimes = ['0-8am', '8am-12am', '12am-8pm','8pm-12pm'];
  timezones = ['UTC -7','UTC -6','UTC -5','UTC -4','UTC -3','UTC -2:30'];
  microphones = ['PC','PS3','PS4','XBox','XB1'];
  pcgames=['Left4Dead2','WorldofWarcraft'];
  ps3games=['Destiny','Diablo3'];
  ps4games=['Destiny','Diablo3'];
  xboxgames=['Destiny','Diablo3','Left4Dead2'];
  xb1games=['Destiny','Diablo3'];
  playstyles=['Any','Casual','Serious','Zealous'];
  destinycharacters=['Hunter','Warlock','Titan'];
  diablo3characters=['Barbarian','Crusader','DemonHunter','Monk','Witch','Doctor','Wizard'];
  left4dead2characters=['coach','Rochelle','Nick','Ellis'];
  worldofwarcraftcharacters=['Warrior','Paladin','Hunter','Rogue','Priest',
              'DeathKnight','Shaman','Mage','Warlock','Monk','Druid','DemonKnight'];
  

  playtimeWeekdays = {
        '0-8am': true,
        '8am-12am': true,
        '12am-8pm': true,
        '8pm-12pm':true
  };
  playtimeWeekends = {
        '0-8am': true,
        '8am-12am': true,
        '12am-8pm': true,
        '8pm-12pm':true
  };
  timezoneWeekdays={
    'UTC -7':true,
    'UTC -6':true,
    'UTC -5':true,
    'UTC -4':true,
    'UTC -3':true,
    'UTC -2:30':true
  };
  timezoneWeekends={
    'UTC -7':true,
    'UTC -6':true,
    'UTC -5':true,
    'UTC -4':true,
    'UTC -3':true,
    'UTC -2:30':true
  };
  
  microphoneStus={
    'PC':true,
    'PS3':true,
    'PS4':true,
    'XBox':true,
    'XB1':true
  };
  pcgameStus={
    'Left4Dead2':true,
    'WorldofWarcraft':true
  };
  ps3gameStus={
    'Destiny':true,
    'Diablo3':true
  };
  ps4gameStus={
    'Destiny':true,
    'Diablo3':true
  };
  xboxgameStus={
    'Destiny':true,
    'Diablo3':true,
    'Left4Dead2':true
  }
  xb1gameStus={
    'Destiny':true,
    'Diablo3':true
  };
  destinycharacterStus={
    'Hunter':true,
    'Warlock':true,
    'Titan':true
  };
  diablo3characterStus={
    'Barbarian':true,
    'Crusader':true,
    'DemonHunter':true,
    'Monk':true,
    'Witch':true,
    'Doctor':true,
    'Wizard':true
  };
  left4dead2characterStus={
    'coach':true,
    'Rochelle':true,
    'Nick':true,
    'Ellis':true
  };
  worldofwarcraftcharacterStus={
    'Warrior':true,
    'Paladin':true,
    'Hunter':true,
    'Rogue':true,
    'Priest':true,
    'DeathKnight':true,
    'Shaman':true,
    'Mage':true,
    'Warlock':true,
    'Monk':true,
    'Druid':true,
    'DemonKnight':true
  }

 
  

  constructor(private profileService: ProfileService,public af: AngularFire,public loginService: LoginService) {
    
        this.loginService.af.auth.subscribe(
      (auth) => {
        if(auth == null) {

        }
        else {
          // Set the Display Name and Email so we can attribute messages to them
          if(auth.google) {
            this.loginService.displayName = auth.google.displayName;
            this.loginService.email = auth.google.email;
            //this.userEmail=this.loginService.email;
          }
          else {
            this.loginService.displayName = auth.auth.email;
            this.loginService.email = auth.auth.email;
            //console.log(this.loginService.displayName);
            //this.userEmail=this.loginService.email;
          }
        }
      }
    );
  }
  
    ngOnInit() {
  }
  
  //PROFILE IMAGE--------------------------------------------------------------------------------
    public imgUrl: string;
    
    fileList : FirebaseListObservable<Image[]>;
    imageList : Observable<Image[]>;
    
    public isImg: boolean;
    
    
    
    
    upload() {
        // Create a root reference
        let storageRef = firebase.storage().ref();

        let success = false;
        // This currently only grabs item 0, TODO refactor it to grab them all
        for (let selectedFile of [(<HTMLInputElement>document.getElementById('file')).files[0]]) {
            console.log(selectedFile);
            // Make local copies of services because "this" will be clobbered
            if (selectedFile.size < imageSize){
              console.log('size smaller than 10000');
              let af = this.af;
              //let folder = "userImg";
              let path = `/userImg/${selectedFile.name}`;
              var iRef = storageRef.child(path);
              iRef.put(selectedFile).then((snapshot) => {
                console.log("Img Uploaded!");
                af.database.list(`/userImg/images/`).push({ path: path, filename: selectedFile.name });
                let downloadUrl = snapshot.downloadURL;
                console.log(downloadUrl);
                this.imgUrl = downloadUrl;
                
                this.isImg = true;
            });
            }
            else{
              console.log('size over 10000');
            }

        }
    
    }
    
    
    
  //creat profile--------------------------------------------------
  updateCheckedPlaytimesWeekdays(playtime,playtimeWeekdays,event){
    this.profileService.updateCheckedOptions(playtime,playtimeWeekdays,event);
  }
  updateCheckedPlaytimesWeekends(playtime,playtimeWeekends,event){
    this.profileService.updateCheckedOptions(playtime,playtimeWeekends,event);
  }
  
  
  TimezoneWeekdays(timezone,timezoneWeekdays,event){
    this.profileService.updateCheckedOptions(timezone,timezoneWeekdays,event);
  }
  TimezoneWeekends(timezone,timezoneWeekends,event){
    this.profileService.updateCheckedOptions(timezone,timezoneWeekends,event);
  }
  
  
  MicrophoneStus(microphone,microphoneStus,event){
    this.profileService.updateCheckedOptions(microphone,microphoneStus,event);
  }
  
  
  PcgameStus(pcgame,pcgameStus,event){
    this.profileService.updateCheckedOptions(pcgame,pcgameStus,event);
  }
  Ps3gameStus(ps3game,ps3gameStus,event){
    this.profileService.updateCheckedOptions(ps3game,ps3gameStus,event);
  }
  Ps4gameStus(ps4game,ps4gameStus,event){
    this.profileService.updateCheckedOptions(ps4game,ps4gameStus,event);
  }
  XboxgameStus(xboxgame,xboxgameStus,event){
    this.profileService.updateCheckedOptions(xboxgame,xboxgameStus,event);
  }
  Xb1gameStus(xb1game,xb1gameStus,event){
    this.profileService.updateCheckedOptions(xb1game,xb1gameStus,event);
  }
  
  
  DestinycharacterStus(destinycharacter,destinycharacterStus,event){
    this.profileService.updateCheckedOptions(destinycharacter,destinycharacterStus,event);
  }
  Diablo3characterStus(diablo3character,diablo3characterStus,event){
    this.profileService.updateCheckedOptions(diablo3character,diablo3characterStus,event);
  }
  Left4Dead2characterStus(left4dead2character,left4dead2characterStus,event){
    this.profileService.updateCheckedOptions(left4dead2character,left4dead2characterStus,event);
  }
  WorldofWarcraftcharacterStus(worldofwarcraftcharacter,worldofwarcraftcharacterStus,event){
    this.profileService.updateCheckedOptions(worldofwarcraftcharacter,worldofwarcraftcharacterStus,event);
  }
  


  
  sendProfile(age,idpc,idps3,idps4,idxbox,idxb1,
  playtimeWeekdays,  playtimeWeekends,timezoneWeekdays,timezoneWeekends,
  microphoneStus,pcgameStus,ps3gameStus,ps4gameStus,xboxgameStus,xb1gameStus,
  DestinyPlayStyle,Diabl3PlayStyle,Left4Dead2PlayStyle,WorldofWarcraftPlayStyle,
  destinycharacterStus,diablo3characterStus,left4dead2characterStus,worldofwarcraftcharacterStus,
  imgUrl,userEmail
  
  ){
    this.profileService.sendProfile(age,idpc,idps3,idps4,idxbox,idxb1,
  playtimeWeekdays,  playtimeWeekends,timezoneWeekdays,timezoneWeekends,
  microphoneStus,pcgameStus,ps3gameStus,ps4gameStus,xboxgameStus,xb1gameStus,
  DestinyPlayStyle,Diabl3PlayStyle,Left4Dead2PlayStyle,WorldofWarcraftPlayStyle,
  destinycharacterStus,diablo3characterStus,left4dead2characterStus,worldofwarcraftcharacterStus,
  imgUrl,userEmail
    );
  }
  


}
