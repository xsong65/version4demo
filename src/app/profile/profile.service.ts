import { Injectable } from '@angular/core';
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';

@Injectable()
export class ProfileService {
  public profiles: FirebaseListObservable<any>;
  //public users: FirebaseListObservable<any>;
  public destiny: FirebaseListObservable<any>;
  public diablo3: FirebaseListObservable<any>;
  public left4dead2: FirebaseListObservable<any>;
  public worldofwarcraft: FirebaseListObservable<any>;
  playlist=[];
  //public displayName: string;
  //public email: string;

  constructor(public af: AngularFire) { 
      this.profiles = this.af.database.list('profile');
      this.destiny = this.af.database.list('Destiny');
      this.diablo3 = this.af.database.list('Diablo3');
      this.left4dead2 = this.af.database.list('Left4Dead2');
      this.worldofwarcraft = this.af.database.list('WorldofWarcraft');
  }
     
     
     /**
   * Saves a profile to the Firebase Realtime Database
   * @param text
   */
   /*
    sendProfile(age) {
        var profile = {
            userage: age,
            //displayName: this.displayName,
            //email: this.email,
            //timestamp: Date.now()
        };
        console.log(profile.userage);
        //this.profiles.push(profile);
    }
    */

    
    updateCheckedOptions(option, optionsMap, event) {
        optionsMap[option] = event.target.checked;
        //array = optionsMap;
        //console.log(array);
        //console.log(optionsMap[option]);
        //console.log(optionsMap);
    }
    /*
    updateOptions(options,optionsMap,optionsChecked) {
        for(var x in optionsMap) {
            if(optionsMap[x]) {
                optionsChecked.push(x);
                console.log(optionsChecked);
            }
        }
        options = optionsChecked;
        console.log(options);
        optionsChecked = [];
    }
    
    */
  sendProfile(age,idpc,idps3,idps4,idxbox,idxb1,
  playtimeWeekdays,  playtimeWeekends,timezoneWeekdays,timezoneWeekends,
  microphoneStus,pcgameStus,ps3gameStus,ps4gameStus,xboxgameStus,xb1gameStus,
  DestinyPlayStyle,Diabl3PlayStyle,Left4Dead2PlayStyle,WorldofWarcraftPlayStyle,
  destinycharacterStus,diablo3characterStus,left4dead2characterStus,worldofwarcraftcharacterStus,
  imgUrl,userEmail
  ) {
      console.log(imgUrl);
      if(imgUrl===undefined){
          imgUrl='https://firebasestorage.googleapis.com/v0/b/proj-ed9c1.appspot.com/o/default.png?alt=media&token=e78bb896-810b-43c0-819b-e69d8106e4ce';
      }
    
    var destinyCnt={
        useridPC: idpc,
        useridPS3: idps3,
        useridPS4: idps4,
        useridXBox: idxbox,
        useridXB1: idxb1,
        userDestinyPlayStyle: DestinyPlayStyle,
        userDestinycharacterStus: destinycharacterStus,
        userimgUrl: imgUrl,
        userEmail:userEmail
    };
    var diablo3Cnt={
        useridPC: idpc,
        useridPS3: idps3,
        useridPS4: idps4,
        useridXBox: idxbox,
        useridXB1: idxb1,
        userDiabl3PlayStyle: Diabl3PlayStyle,
        userDiablo3characterStus: diablo3characterStus,
        userimgUrl: imgUrl,
        userEmail:userEmail
    };
    var left4dead2Cnt={
        useridPC: idpc,
        useridPS3: idps3,
        useridPS4: idps4,
        useridXBox: idxbox,
        useridXB1: idxb1,
        userLeft4Dead2PlayStyle: Left4Dead2PlayStyle,
        userLeft4dead2characterStus:left4dead2characterStus,
        userimgUrl: imgUrl,
        userEmail:userEmail
    };
    var worldofwarcraftCnt={
        useridPC: idpc,
        useridPS3: idps3,
        useridPS4: idps4,
        useridXBox: idxbox,
        useridXB1: idxb1,
        userWorldofWarcraftPlayStyle: WorldofWarcraftPlayStyle,
        userWorldofwarcraftcharacterStus:worldofwarcraftcharacterStus,
        userimgUrl: imgUrl,
        userEmail:userEmail
    };
    
    
    //console.log(destinycharacterStus);
    //console.log(destinycharacterStus.Hunter);
    if (destinycharacterStus.Hunter ||destinycharacterStus.Warlock||destinycharacterStus.Titan ){
        this.destiny.push(destinyCnt);
        this.playlist.push('Destiny');
    }
    if(diablo3characterStus.Barbarian||diablo3characterStus.Crusader||
        diablo3characterStus.DemonHunter||diablo3characterStus.Monk||diablo3characterStus.Witch||
        diablo3characterStus.Doctor||diablo3characterStus.Wizard){
            this.diablo3.push(diablo3Cnt);
            this.playlist.push('Diablo 3');
    }
    if(left4dead2characterStus.coach||left4dead2characterStus.chelle||
        left4dead2characterStus.Nick||left4dead2characterStus.Ellis){
            this.left4dead2.push(left4dead2Cnt);
            this.playlist.push('Left 4 Dead 2');
        }
    if(worldofwarcraftcharacterStus.Warrior||worldofwarcraftcharacterStus.Paladin||
        worldofwarcraftcharacterStus.Hunter||worldofwarcraftcharacterStus.Rogue||
        worldofwarcraftcharacterStus.Priest||worldofwarcraftcharacterStus.DeathKnight||
        worldofwarcraftcharacterStus.Shaman||worldofwarcraftcharacterStus.Mage||
        worldofwarcraftcharacterStus.Warlock||worldofwarcraftcharacterStus.Monk||
        worldofwarcraftcharacterStus.Druid||worldofwarcraftcharacterStus.DemonKnight){
            this.worldofwarcraft.push(worldofwarcraftCnt);
            this.playlist.push('World of Warcraft');
        }
        
    var profile = {
      userage: age,
      //userdisplayName: this.displayName,
      //useremail: this.email,
      useridPC: idpc,
      useridPS3: idps3,
      useridPS4: idps4,
      useridXBox: idxbox,
      useridXB1: idxb1,
      userplaytimeWeekdays: playtimeWeekdays,
      userplaytimeWeekends:  playtimeWeekends,
      usertimezoneWeekdays: timezoneWeekdays,
      usertimezoneWeekends: timezoneWeekends,
      usermicrophoneStus: microphoneStus,
      userpcgameStus: pcgameStus,
      userps3gameStus: ps3gameStus,
      userps4gameStus: ps4gameStus,
      userxboxgameStus: xboxgameStus,
      userxb1gameStus: xb1gameStus,
      userDestinyPlayStyle: DestinyPlayStyle,
      userDiabl3PlayStyle: Diabl3PlayStyle,
      userLeft4Dead2PlayStyle: Left4Dead2PlayStyle,
      userWorldofWarcraftPlayStyle: WorldofWarcraftPlayStyle,
      userDestinycharacterStus: destinycharacterStus,
      userDiablo3characterStus: diablo3characterStus,
      userLeft4dead2characterStus:left4dead2characterStus,
      userWorldofwarcraftcharacterStus:worldofwarcraftcharacterStus,
      userimgUrl: imgUrl,
      userEmail:userEmail,
      userPlaylist: this.playlist
    };
    console.log(profile);
    //console.log(playtimesDestiny);
    this.profiles.push(profile);
    
  }


}
