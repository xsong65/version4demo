import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  games: FirebaseListObservable<any[]>;
  purposes: FirebaseListObservable<any[]>;
  users: FirebaseListObservable<any[]>;
  userCnt;
  cnt;


  constructor(af: AngularFire) {
    this.purposes = af.database.list('/purposes');
    //this.games = af.database.list('/games');
    this.users = af.database.list('/profile');
    console.log(this.users);
    this.users.subscribe((res) => {
    this.userCnt = res;
    console.log(this.userCnt.length);
    this.cnt = this.userCnt.length
    })
  }

  ngOnInit() {
  }

}
